# MQTT-InfluxDB-Grafana Logger and Visualizer

This project provides a comprehensive solution for logging MQTT data into InfluxDB and visualizing it with Grafana. With this setup, you can easily capture, store, and explore your MQTT data streams in a visually appealing and insightful way.

## Features

- MQTT data logging: Collect MQTT data from various sources.
- InfluxDB storage: Store MQTT data in a highly efficient and scalable InfluxDB database.
- Grafana visualization: Create stunning visualizations and dashboards with Grafana.
- Real-time monitoring: Keep an eye on your MQTT data in real-time.
- Historical analysis: Analyze historical MQTT data trends.


## Grafana 

![Grafana Demo](images/Grafana.png)

