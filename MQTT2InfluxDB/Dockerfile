FROM python:3.10-alpine

RUN apk update \
    && apk upgrade --available \
    && apk add build-base linux-headers \
    && apk add --no-cache python3-dev bash git gettext curl openssl \
    && apk add --no-cache libffi-dev openssl-dev libxml2-dev libxslt-dev

LABEL maintainer="Florian Guschlbauer" \
      description="MQTT to InfluxDB Bridge"
      
COPY . /app
WORKDIR /app

ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN /opt/venv/bin/python3 -m pip install --upgrade pip
COPY requirements.txt /
RUN pip3 install -r /requirements.txt

CMD ["python3", "-u", "main.py"]
