from typing import Type
import paho.mqtt.client as mqtt
import ssl
import argparse
import json
import re
from influxdb import InfluxDBClient

influxdb_address = 'influxdb'
influxdb_database = 'home_db'
influxdb_database_string = 'home_db_string'

influxdb_client = InfluxDBClient(influxdb_address, 8086)

mqtt_address = 'mosquitto'
mqtt_client_id = 'MQTT2InfluxDB'
mqtt_topic = '#'  # [Type]/[value]


def on_connect(client, userdata, flags, rc):
    print('Connected with result code ' + str(rc))
    client.subscribe(mqtt_topic)


def on_message(client, userdata, msg):
    dataFormatting(msg.topic, (msg.payload))


def dataFormatting(topic, payload):
    topic = re.split("/", topic)
    system = topic[0]
    service = topic[2] if topic[1] == "System" else topic[len(topic) - 1]
    variable = topic[4] if topic[1] == "System" else topic[len(topic) - 1]

    try:
        payload = float(payload.decode('utf-8'))
        sendSensorDataToInfluxDB(system, service, variable, payload)
    except:
        payload = str(payload.decode('utf-8'))
        pass


def sendSensorDataToInfluxDB(system, service, variable, payload):
    json_body = [
        {
            'measurement': variable,
            'tags': {
                'location': service,
                'system': system
            },
            'fields': {
                'value': float(payload)
            }
        }
    ]
    try:
        influxdb_client.switch_database(influxdb_database)
        influxdb_client.write_points(json_body, database=influxdb_database)
    except Exception as error:
        print("Error saving to Influxdb:")
        print(error)


def initInfluxDBDatabase():
    databases = influxdb_client.get_list_database()
    if len(list(filter(lambda x: x['name'] == influxdb_database, databases))) == 0:
        influxdb_client.create_database(influxdb_database)

    influxdb_client.switch_database(influxdb_database)


def main():
    mqtt_client = mqtt.Client(mqtt_client_id)
    # mqtt_client.username_pw_set(MQTT_USER, MQTT_PASSWORD)
    mqtt_client.on_connect = on_connect
    mqtt_client.on_message = on_message
    mqtt_client.connect(mqtt_address, 1883, 5)
    mqtt_client.loop_forever()
    print("Started Thread MQTT")


if __name__ == '__main__':
    initInfluxDBDatabase()
    print("Initialized Influx_DB")
    print("Start IO_Loop")
    main()
